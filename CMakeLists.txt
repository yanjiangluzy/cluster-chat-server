cmake_minimum_required(VERSION 3.0)

project(chat)

# 头文件路径
include_directories(${PROJECT_SOURCE_DIR}/include)
include_directories(${PROJECT_SOURCE_DIR}/include/server)
include_directories(${PROJECT_SOURCE_DIR}/include/server/db)
include_directories(${PROJECT_SOURCE_DIR}/include/server/model)    
include_directories(${PROJECT_SOURCE_DIR}/include/server/redis)           
include_directories(${PROJECT_SOURCE_DIR}/third_part)


# 调试选项
set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -g)

# 输出的路径
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)


# 添加子目录
add_subdirectory(src)
