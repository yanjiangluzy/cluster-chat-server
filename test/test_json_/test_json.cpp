#include <iostream>
#include <cstdio>
#include <vector>
#include <map>
#include <muduo/base/Logging.h>
#include "json.hpp"


using namespace std;
using json = nlohmann::json;
using namespace muduo;


// int main()
// {
//     // {"msg_id": 1, "name":"张乐诗", "password":"123456"}
//     string str = "{\"msg_id\": 1, \"name\":\"张乐诗\", \"password\":\"123456\"}";
//     cout << str << endl;

//     json js = json::parse(str);

//     printf("id: %d, name: %s, password: %s\n", js["msg_id"].get<int>(), js["name"].get<string>().c_str(), js["password"].get<string>().c_str());

//     return 0;
// }

// // 正常序列化
// void test_1()
// {
//     json js;
//     // 添加数组
//     js["id"] = {1,2,3,4,5};
//     // 添加key-value
//     js["name"] = "zhang san";
//     // 添加对象
//     js["msg"]["zhang san"] = "hello world";
//     js["msg"]["liu shuo"] = "hello china";
//     // 上面等同于下面这句一次性添加数组对象
//     js["msg"] = {{"zhang san", "hello world"}, {"liu shuo", "hello china"}};

//     js["zls"]["三围"]["腰"] = "11";
//     js["zls"]["三围"]["腿"] = "20";
//     js["zls"]["三围"]["熊"] = "50";


//     cout << js << endl;
// }
// // 序列化容器
// void test_2()
// {
//     // 直接序列化一个vector容器
//     json js;
//     vector<int> vec;
//     vec.push_back(1);
//     vec.push_back(2);
//     vec.push_back(5);
//     js["list"] = vec;
//     // 直接序列化一个map容器
//     map<int, string> m;
//     m.insert({1, "黄山"});
//     m.insert({2, "华山"});
//     m.insert({3, "泰山"});
//     js["path"] = m;
//     cout<<js<<endl;
// }

// // 反序列化，从字符串到json容器
// void test_3()
// {
//     json js;
//         // 添加数组
//     js["id"] = {1,2,3,4,5};
//     // 添加key-value
//     js["name"] = "zhang san";
//     // 添加对象
//     js["msg"]["zhang san"] = "hello world";
//     js["msg"]["liu shuo"] = "hello china";
//     // 上面等同于下面这句一次性添加数组对象
//     js["msg"] = {{"zhang san", "hello world"}, {"liu shuo", "hello china"}};

//     js["zls"]["三围"]["腰"] = "11";
//     js["zls"]["三围"]["腿"] = "20";
//     js["zls"]["三围"]["熊"] = "50";

//     vector<int> vec;
//     vec.push_back(1);
//     vec.push_back(2);
//     vec.push_back(5);
//     js["list"] = vec;
//     // 直接序列化一个map容器
//     map<int, string> m;
//     m.insert({1, "黄山"});
//     m.insert({2, "华山"});
//     m.insert({3, "泰山"});
//     js["path"] = m;

//     string jsonstr = js.dump();   // dump是序列化成字符串

//     cout<<"jsonstr:"<<jsonstr<<endl;

//     // 模拟从网络接收到json字符串，通过json::parse函数把json字符串专程json对象
//     json js2 = json::parse(jsonstr);
//     // 直接取key-value
//     string name = js2["name"];
//     cout << "name:" << name << endl;
//     // 直接反序列化vector容器
//     vector<int> v = js2["list"];
//     for(int val : v)
//     {
//         cout << val << " ";
//     }
//     cout << endl;
//     // 直接反序列化map容器
//     map<int, string> m2 = js2["path"];
//     for(auto p : m2)
//     {
//         cout << p.first << " " << p.second << endl;
//     }
//     cout << endl;
// }

// int main()
// {
//     // test_1();
//     // test_2();
//     test_3();
//     return 0;
// }