#include <cstdio>
#include "FriendModel.h"
#include "db.h"

// 添加好友
bool FriendModel::addFriend(int src, int target)
{
    char sql[1024];
    snprintf(sql, sizeof sql, "insert into friend(userid, friendid) values(%d, %d);", src, target);

    MySQL mysql;

    if (!mysql.connect())
    {
        return false;
    }

    if (!mysql.update(sql))
    {
        return false;
    }

    return true;
}

// 删除好友
bool FriendModel::delFriend(int src, int target)
{
    char sql[1024];
    snprintf(sql, sizeof sql, "delete from friend where userid=%d and friendid=%d;", src, target);

    MySQL mysql;

    if (!mysql.connect())
    {
        return false;
    }

    if (!mysql.update(sql))
    {
        return false;
    }

    return true;
}

// 查询好友
vector<int> FriendModel::getFriend(int src)
{
    char sql[1024];
    snprintf(sql, sizeof sql, "select * from friend where userid=%d", src);

    MySQL mysql;

    if (!mysql.connect())
    {
        return vector<int>();
    }

    MYSQL_RES* res = mysql.query(sql);
    if (res == nullptr)
    {
        return vector<int>();
    }

    // 查询成功
    vector<int> ret;
    MYSQL_ROW row;
    while (row = mysql_fetch_row(res))
    {
        ret.push_back(atoi(row[1]));
    }

    return ret;
}