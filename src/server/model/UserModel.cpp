#include "UserModel.h"
#include "db.h"
#include <cstring>

bool UserModel::insert(User &user)
{
    // 拼接sql
    char sql[1024];
    snprintf(sql, sizeof sql, "insert into user(name, password, state) values('%s', %s, '%s')",
             user.getName().c_str(), user.getPasswd().c_str(), user.getState().c_str());

    MySQL mysql;

    if (!mysql.connect())
        return false;

    if (mysql.update(sql))
    {
        user.setId(mysql_insert_id(mysql.getConn()));
        return true;
    }
    else
    {
        return false;
    }
}

bool UserModel::update(User user)
{
    char sql[1024];
    snprintf(sql, sizeof sql, "update user set name='%s', password='%s', state='%s' where id=%s",
             user.getName().c_str(), user.getPasswd().c_str(), user.getState().c_str(),
             std::to_string(user.getId()).c_str());

    MySQL mysql;

    if (!mysql.connect())
        return false;

    if (mysql.update(sql))
    {
        return true;
    }
    else
    {
        return false;
    }
}

User UserModel::query(User &user)
{
    char sql[1024];
    snprintf(sql, sizeof sql, "select * from user where id=%s;", std::to_string(user.getId()).c_str());

    MySQL mysql;

    if (!mysql.connect())
        return User();
    MYSQL_RES *res = mysql.query(sql);
    unsigned int col = mysql_num_fields(res);

    if (res != nullptr)
    {
        MYSQL_ROW row;
        User user;
        while (row = mysql_fetch_row(res))
        {
            user.setId(atoi(row[0]));
            user.setName(row[1]);
            user.setPasswd(row[2]);
            user.setState(row[3]);
        }


        return user;
    }

    return User();
}