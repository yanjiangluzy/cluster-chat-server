#include "OffLineMessageModel.h"

bool OffLineMessageModel::insert(int id, string msg)
{
    char sql[1024];
    LOG_INFO << "=======ssss=======";
    LOG_INFO << "MSG: " << msg;
    LOG_INFO << "=======ssss=======";
    // 自动给msg添加上转义字符

    snprintf(sql, sizeof(sql), "insert into offlinemessage values(%d, '%s');", id, msg.c_str());


    MySQL mysql;

    if (!mysql.connect()) return false;

    if (mysql.update(sql))
    {
        return true;
    }

    return false;

}

bool OffLineMessageModel::del(int id)
{
    char sql[1024];
    snprintf(sql, sizeof sql, "delete from offlinemessage where userid= %d", id);

    MySQL mysql;

    if (!mysql.connect())
        return false;

    if (mysql.update(sql))
    {
        return true;
    }

    return false;
}

/**
 * @brief 根据id查询用户的离线信息
 * @param id 
 * @return 
 */
std::vector<string> OffLineMessageModel::query(int id)
{
    char sql[1024];
    snprintf(sql, sizeof sql, "select * from offlinemessage where userid=%d", id);


    std::vector<string> res;
    MySQL mysql;

    if (!mysql.connect())
        return res;

    MYSQL_RES* ret = mysql.query(sql);

    if (nullptr != ret)
    {
        // 查询成功
        MYSQL_ROW row;
        while (row = mysql_fetch_row(ret))
        {
            res.push_back(row[1]);
        } 
    }

    return res;
}