#include "ChatServer.h"
#include "ChatService.h"

#include <functional>

ChatServer::ChatServer(EventLoop *loop,
                       const InetAddress &listenAddr,
                       const string &nameArg)
    : m_server(loop, listenAddr, nameArg), m_loop(loop)
{
    // 注册链接回调
    m_server.setConnectionCallback(std::bind(&ChatServer::onConnection, this, std::placeholders::_1));

    // 注册读写回调
    m_server.setMessageCallback(std::bind(&ChatServer::onMessage, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

    // 设置线程数量   -- 后续通过配置文件修改
    m_server.setThreadNum(4);
}

void ChatServer::onConnection(const TcpConnectionPtr &conn)
{
    if (!conn->connected())
    {
        // 客户端链接断开
        ChatService::getInstance()->hanleWrongConnection(conn);
        return;
    }
}

void ChatServer::onMessage(const TcpConnectionPtr &conn, Buffer *buf, Timestamp time)
{
    string message = buf->retrieveAllAsString();

    json js;
    try
    {
        js = json::parse(message);
    }
    catch(const std::exception& e)
    {
        LOG_INFO << e.what();
        return;
    }

    try
    {
        msg_handler handler = ChatService::getInstance()->getMsgHandler(js["msg_id"].get<int>());
        handler(conn, js, time);
    }
    catch(const std::exception& e)
    {
        // 如果msg_id不是int，不给返回
        LOG_INFO << e.what();
        return;
    }
}

void ChatServer::start()
{
    m_server.start();
}
