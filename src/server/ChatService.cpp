#include "ChatService.h"
#include "OffLineMessageModel.h"

// 定义
ChatService *ChatService::getInstance()
{
    static ChatService service;
    return &service;
}

// 初始化
ChatService::ChatService()
{
    m_handler_map[MessageID::LOGIN] = std::bind(&ChatService::login, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    m_handler_map[MessageID::REGISTER] = std::bind(&ChatService::register_, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    m_handler_map[MessageID::ONE_CHAT] = std::bind(&ChatService::oneChat, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);

    // 链接到redis
    if (m_redis.connect())
    {
        // 设置好回调函数--线程获取到redis服务器的消息后，上报到service层
        m_redis.init_notify_handler(std::bind(&ChatService::hanlderRedis, this, std::placeholders::_1, std::placeholders::_2));
    }
}

/**
 * @brief 处理redis回调
 * @param id
 * @param msg
 */
void ChatService::hanlderRedis(int id, string msg)
{
    // 这里从redis收到了来自其他服务器对于该用户的消息
    // 有可能在向redis取消息的时候，用户下线，所以需要验证一下
    std::lock_guard<std::mutex> lock(m_mutex);
    for (auto it = m_user_to_conn.begin(); it != m_user_to_conn.end(); ++it)
    {
        if (it->second.getId() == id)
        {
            // 找到了该用户

            LOG_INFO << msg;
            it->first->send(msg);
            return;
        }
    }

    // 找不到，添加到离线消息
    OffLineMessageModel::insert(id, msg);
}

/**
 * @brief 私聊
 * @brief 客户端应该发送的数据格式 {"msg_id":2, "from": id, "to": id, "msg": msg}
 * @param id 用户账号
 * @param msg 具体消息
 */
void ChatService::oneChat(const TcpConnectionPtr &conn, json &js, Timestamp time)
{


    int to_id = js["to"].get<int>();
    string msg = js["msg"].get<string>();


    // 找寻目标用户
    bool is_online = false;

    {
        std::lock_guard<std::mutex> lock(m_mutex);
        for (auto it = m_user_to_conn.begin(); it != m_user_to_conn.end(); ++it)
        {
            if (it->second.getId() == to_id)
            {
                // 在线用户 -- 且在当前服务器上
                is_online = true;
 
                it->first->send(js.dump());

                break;
            }
        }
    }

    if (!is_online)
    {
        // 有可能在其他服务器上
        User user;
        user.setId(to_id);
        User to_user = UserModel::query(user);
        if (to_user.getState() == "online")
        {
            // 说明在其他服务器上
            // 那么将消息推送到通道上
            m_redis.publish(to_id, js.dump());
        }
        else
        {
            // 说明真的不在线
            // 离线用户，存储到离线消息队列
            std::cout << js;
            std::string msg = js.dump();
            if (!OffLineMessageModel::insert(to_id, js.dump()))
            {
                json response;
                response["msg_id"] = ONE_CHAT_ACK;
                response["err_code"] = FAIL_DB;
                response["err_msg"] = code_to_reason[FAIL_DB];
                conn->send(response.dump());
                return;
            }
        }
    }

    return;
}

/**
 * @brief 登录
 * @param conn
 * @param js
 * @param time
 */
void ChatService::login(const TcpConnectionPtr &conn, json &js, Timestamp time)
{

    json response;
    response["msg_id"] = MessageID::LOGIN_ACK;

    /**
     * @brief 检查字段是否缺失
     *
     */
    if (js.find("password") == js.end() || js.find("id") == js.end())
    {
        response["err_code"] = ErrorCode::TOO_LITTLE_PARA_JSON;
        response["err_msg"] = code_to_reason[ErrorCode::TOO_LITTLE_PARA_JSON];
        conn->send(response.dump());
        return;
    }

    User user;

    /**
     * @brief 检查JSON值是否是满足要求的类型
     */
    try
    {
        LOG_INFO << js["password"].get<string>() << ", " << js["id"].get<int>();
        user.setPasswd(js["password"].get<string>());
        user.setId(js["id"].get<int>());
    }
    catch (const std::exception &e)
    {
        LOG_INFO << e.what();
        response["err_code"] = ErrorCode::TRANSLATE_FAILE_JSON;
        response["err_msg"] = code_to_reason[ErrorCode::TRANSLATE_FAILE_JSON] + "\n" + e.what();
        conn->send(response.dump());
        return;
    }

    User r_user = UserModel::query(user);

    if (r_user.getId() != -1 && r_user.getId() == user.getId() && r_user.getPasswd() == user.getPasswd())
    {
        if (r_user.getState() == "online")
        {
            LOG_INFO << "the user: " << r_user.getName() << "login again";
            response["err_code"] = ErrorCode::FAIL_LOGIN_AGAIN;
            response["err_msg"] = code_to_reason[ErrorCode::FAIL_LOGIN_AGAIN];
        }
        else
        {
            LOG_INFO << "the user " << r_user.getName() << " login success";
            response["err_code"] = ErrorCode::OK;
            response["err_msg"] = code_to_reason[ErrorCode::OK];
            response["name"] = r_user.getName();
            response["id"] = r_user.getId();

            // 更新用户在线状态
            r_user.setState("online");
            UserModel::update(r_user);

            // 添加到上线用户
            {
                // 保证线程安全
                std::lock_guard<std::mutex> lock(m_mutex);
                m_user_to_conn[conn] = r_user;
            }

            // 查看当前用户是否存在离线消息，如果有，那么就发送之
            std::vector<string> msgs = OffLineMessageModel::query(r_user.getId());
            if (!msgs.empty())
            {
                response["offlinemsg"] = msgs;
                // 读取完毕删除
                OffLineMessageModel::del(r_user.getId());
            }

            // 在redis中订阅所有有关于该用户的消息
            m_redis.subscribe(r_user.getId());
        }
    }
    else
    {
        response["err_code"] = ErrorCode::FAIL_IDENTIFY;
        response["err_msg"] = code_to_reason[ErrorCode::FAIL_IDENTIFY];
    }

    conn->send(response.dump());
    return;
}

/**
 * @brief 注册
 * @param conn
 * @param js
 * @param time
 */
void ChatService::register_(const TcpConnectionPtr &conn, json &js, Timestamp time)
{

    json response;
    response["msg_id"] = MessageID::REGISTER_ACK;

    if (js.find("password") == js.end() || js.find("name") == js.end())
    {
        response["err_code"] = ErrorCode::TOO_LITTLE_PARA_JSON;
        response["err_msg"] = code_to_reason[ErrorCode::TOO_LITTLE_PARA_JSON];
        conn->send(response.dump());
        return;
    }

    User user;
    try
    {
        LOG_INFO << js["name"].get<string>() << ", " << js["password"].get<string>();
        user.setPasswd(js["password"].get<string>());
        user.setName(js["name"].get<string>());
    }
    catch (const std::exception &e)
    {
        LOG_INFO << e.what();
        response["err_code"] = ErrorCode::FAIL_IDENTIFY;
        response["err_msg"] = code_to_reason[ErrorCode::FAIL_IDENTIFY] + "\n" + e.what();

        conn->send(response.dump());
        return;
    }

    if (UserModel::insert(user))
    {
        LOG_INFO << "register success, new user id is " << user.getId();
        response["err_code"] = ErrorCode::OK;
        response["err_msg"] = code_to_reason[ErrorCode::OK];
        response["id"] = std::to_string(user.getId());
        conn->send(response.dump());
        return;
    }

    response["err_code"] = ErrorCode::FAIL_DB;
    response["err_msg"] = code_to_reason[ErrorCode::FAIL_DB];
    conn->send(response.dump());
    return;
}

// 单聊服务实现 TODO

/**
 * @brief 获取事件处理器
 * @param msg_id
 * @return
 */
msg_handler ChatService::getMsgHandler(int msg_id)
{
    auto it = m_handler_map.find(msg_id);
    if (it == m_handler_map.end())
    {
        return [=](const TcpConnectionPtr &conn, json &js, Timestamp time)
        {
            LOG_ERROR << "msg_id: " << msg_id << " can not find handler...";
        };
    }

    return it->second;
}

void ChatService::hanleWrongConnection(const TcpConnectionPtr &conn)
{
    User user = m_user_to_conn[conn];
    {
        // 从服务层的结构中删除掉该用户
        std::lock_guard<std::mutex> lock(m_mutex);
        m_user_to_conn.erase(m_user_to_conn.find(conn));
    }

    // 从数据库中修改该用户的在线状态
    user.setState("offline");
    UserModel::update(user);

    // 取消订阅redis
    m_redis.unsubscribe(user.getId());
}

/**
 * @brief 服务器退出, 下线所有用户
 */
void ChatService::resetUserState()
{
    for (auto it = m_user_to_conn.begin(); it != m_user_to_conn.end(); ++it)
    {
        it->second.setState("offline");
        UserModel::update(it->second);

        // 取消redis订阅
        m_redis.unsubscribe(it->second.getId());
    }
}