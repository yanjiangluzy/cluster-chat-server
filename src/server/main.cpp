#include "ChatServer.h"
#include "ChatService.h"

#include <iostream>
#include <signal.h>


void handler(int signum)
{
    ChatService::getInstance()->resetUserState();
    exit(0);   
}



int main(int argc, char* argv[])
{

    if (argc != 3) exit(1);

    signal(SIGINT, handler);
    signal(SIGTSTP, handler);

    EventLoop loop;
    InetAddress inet_addr(argv[1], atoi(argv[2]));
    ChatServer server(&loop, inet_addr, "ChatServer");

    server.start();
    loop.loop();

    return 0;
}