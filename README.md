# 集群聊天服务器

#### 介绍
集群聊天服务，采用nginx充当网关，redis作为消息队列提供服务器间通信服务

### 依赖
boost库
muduo
yaml-cpp
json for modern c++
mysql
redis
nginx