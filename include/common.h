#ifndef __COMMON_H__
#define __COMMON_H__

#include <unordered_map>
#include <string>

using std::string;
using std::unordered_map;

/**
 * @brief 通信消息分类
 */
enum MessageID
{
    LOGIN,        // 登录
    REGISTER,     // 注册
    ONE_CHAT,     // 私聊
    ONE_CHAT_ACK, // 私聊回应
    ADD_FRIEND,   // 添加好友
    DEL_FRIEND,   // 删除好友
    GET_FRIEND,   // 查询好友
    LOGIN_ACK,    // 登录消息的回应
    REGISTER_ACK, // 注册消息的回应
    LOGIN_OUT     // 退出
};

/**
 * @brief 返回给客户端的错误码
 */
enum ErrorCode
{
    TOO_LITTLE_PARA_JSON = 400, // JSON数据段过少
    TRANSLATE_FAILE_JSON,       // JSON转换失败: js["xxx"].get<T>()失败了
    FAIL_DB,                    // 数据库出问题了
    FAIL_IDENTIFY,              // 用户身份验证没有通过
    FAIL_LOGIN_AGAIN,           // 重复登录
    OK = 500                    // ok
};

/**
 * @brief 错误码到错误原因的转换
 *
 */
static unordered_map<int, string> code_to_reason = {
#define XX(code, str) \
    {                 \
        code, #str    \
    }

    XX(ErrorCode::TOO_LITTLE_PARA_JSON, "JSON数据段过少"),
    XX(ErrorCode::TRANSLATE_FAILE_JSON, "JSON转换失败, 请检查传入的值是否是正确的类型"),
    XX(ErrorCode::FAIL_DB, "数据库未响应"),
    XX(ErrorCode::FAIL_IDENTIFY, "身份验证错误, 请确认你的账号和密码"),
    XX(ErrorCode::FAIL_LOGIN_AGAIN, "当前用户已经登录, 请勿重复登录"),
    XX(ErrorCode::OK, "操作成功")

#undef XX
};

#endif