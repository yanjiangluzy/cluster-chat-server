#ifndef __CHATSERVER_H__
#define __CHATSERVER_H__

#include <iostream>
#include <muduo/net/TcpServer.h>
#include <muduo/base/Logging.h>
#include <boost/bind.hpp>
#include <muduo/net/EventLoop.h>
#include "json.hpp"

using json = nlohmann::json;

using namespace muduo;
using namespace muduo::net;


class ChatServer
{
public:
    ChatServer(EventLoop *loop,
               const InetAddress &listenAddr,
               const string &nameArg);

    void start();

    void onConnection(const TcpConnectionPtr &conn);

    void onMessage(const TcpConnectionPtr &conn, Buffer *buf, Timestamp time);

private:
    TcpServer m_server;
    EventLoop *m_loop;
};

#endif
