#ifndef __CHATSERVICE_H__

#define __CHATSERVICE_H__
#include <iostream>
#include <unordered_map>
#include <functional>
#include <mutex>
#include <map>
#include <vector>
#include <muduo/net/TcpServer.h>
#include <muduo/base/Logging.h>
#include <boost/bind.hpp>
#include <muduo/net/EventLoop.h>
#include "common.h"
#include "User.hpp"
#include "UserModel.h"
#include "json.hpp"
#include "redis.h"

using namespace muduo;
using namespace muduo::net;
using json = nlohmann::json;

using msg_handler = std::function<void(const TcpConnectionPtr, json&, Timestamp)>;

// 存储消息和对应处理方法的映射
class ChatService
{
public:
    static ChatService* getInstance();


    // 回调函数
    void login(const TcpConnectionPtr &conn, json &js, Timestamp time);

    void register_(const TcpConnectionPtr &conn, json &js, Timestamp time);

    void oneChat(const TcpConnectionPtr &conn, json &js, Timestamp time);

    void addFriend(const TcpConnectionPtr &conn, json &js, Timestamp time);

    void delFriend(const TcpConnectionPtr &conn, json &js, Timestamp time);

    void getFriend(const TcpConnectionPtr &conn, json &js, Timestamp time); 

    // 获取事件处理器
    msg_handler getMsgHandler(int msg_id);

    // 处理客户端异常断开链接
    void hanleWrongConnection(const TcpConnectionPtr &conn);

    // 服务器主动退出 -- ctrl c后, 强制下线所有用户
    void resetUserState();

    // 设置从redis中获取到消息的回调函数
    void hanlderRedis(int id, string msg);

private:

    ChatService();

    // 保存上线用户对应的链接
    // 这里用unordered_map不行，map可以 TODO
    std::map<TcpConnectionPtr, User> m_user_to_conn;

    // 保证多线程添加在线用户的线程安全问题
    std::mutex m_mutex;

    // 消息类型 -> 事件处理器
    std::unordered_map<int, msg_handler> m_handler_map;

    // redis
    Redis m_redis;
};

#endif