#ifndef __FRIENDMODEL_H__
#define __FRIENDMODEL_H__
#include <vector>

using std::vector;

class FriendModel
{
public:
    // 添加好友
    bool addFriend(int src, int target);

    // 删除好友
    bool delFriend(int src, int target);

    // 查询好友
    vector<int> getFriend(int src);
private:
};

#endif