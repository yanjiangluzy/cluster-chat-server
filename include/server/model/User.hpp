#ifndef __USER_HPP__
#define __USER_HPP__
#include <string>

using std::string;


/**
 * @brief User表的实体映射
 */
class User
{
public:
    User(const std::string& name = "xxx", const string& passwd="xxx", int id = -1, string state = "offline")
        : m_name(name), m_passwd(passwd), m_id(id), m_state(state)
    {}

    /**
     * @brief set方法
     */
    void setId(int id) { m_id = id; }
    void setName(const string& name) { m_name = name; }
    void setPasswd(const string &passwd) { m_passwd = passwd; }
    void setState(string state) { if(state == "online" || state == "offline") m_state = state; }

    /**
     * @brief get方法
     */
    int getId() { return m_id; }
    const string& getName() { return m_name; }
    const string& getPasswd() { return m_passwd; }
    string getState() { return m_state; }

private:
    int m_id;
    string m_name;
    string m_passwd;
    string m_state;
};

#endif