#ifndef __OFFLINEMESSAGE_H__
#define __OFFLINEMESSAGE_H__
#include "common.h"
#include "db.h"
#include <vector>

class OffLineMessageModel
{
public:
    // 插入消息
    static bool insert(int id, string msg);

    // 删除消息
    static bool del(int id);

    // 查询某用户的离线消息
    static std::vector<string> query(int id);
private:
};

#endif