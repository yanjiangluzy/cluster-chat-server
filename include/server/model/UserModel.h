#ifndef __USERMODEL_H__
#define __USERMODEL_H__
#include "User.hpp"

/**
 * @brief 定义对User表的操作
 */
class UserModel
{
private:
    /* data */
public:
    // 插入用户
    static bool insert(User& user);
    // 查询用户
    static User query(User& user);
    // 删除用户
    static void dele(User& user);
    // 更新用户
    static bool update(User user);
};


#endif