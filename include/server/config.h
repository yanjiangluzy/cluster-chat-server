#ifndef __CONFIG_H__
#define __CONFIG_H__
#include <memory>
#include <map>
#include <string>
#include <boost/lexical_cast.hpp>
#include <muduo/base/Logging.h>

using namespace muduo;

/**
 * @brief 存储配置项的值的基类
 */
class ConfigVarBase
{
public:
    typedef std::shared_ptr<ConfigVarBase> ptr;

    ConfigVarBase(const std::string &name, const std::string &desc)
        : m_name(name), m_description(desc)
    {
    }

    virtual ~ConfigVarBase() {}

    /**
     * @brief 把string转化成自己的类成员
     * @param val
     * @return
     */
    virtual bool fromString(const std::string &val) = 0;

    /**
     * @brief 将配置项转化成字符串
     * @return
     */
    virtual std::string toString() = 0;

private:
    std::string m_name;
    std::string m_description;
};

// 定义出具体的不同值得类
// 该类可以转换基础类型, 如int, char等
template <class T>
class ConfigVar : public ConfigVarBase
{
public:
    typedef std::shared_ptr<ConfigVar> ptr;

    ConfigVar(const std::string &name, const std::string &desc, T val)
        : ConfigVarBase(name, desc), m_val(val)
    {
    }

    /**
     * @brief 重写
     * @return 
     */
    std::string toString() override
    {
        try
        {
            return boost::lexical_cast<std::string>(m_val);
        }
        catch (const std::exception &e)
        {
            LOG_ERROR << "configVar toString error" << e.what();
        }
    }

    /**
     * @brief 重写
     * @param str 
     * @return 
     */
    bool fromString(const std::string &str) override
    {
        try
        {
            m_val = boost::lexical_cast<T>(str);
        }
        catch (const std::exception &e)
        {
            LOG_ERROR << "configVar fromString error" << e.what();
        }
    }

    const T& getVal() { return m_val; }

private:
    T m_val; // 具体得配置项得值
};

// Config得管理类
// 设计成单例
class Config
{
public:
    // 由名称映射到具体得ConfigVar项
    typedef std::map<std::string, ConfigVarBase> config_map;

    // 找寻是否存在该项得解析器，如果没有，那么创建
    template <class T>
    static typename ConfigVar<T>::ptr lookUp(const std::string &name,
                                           const std::string &desc,
                                           T default_val)
    {
        typename ConfigVar<T>::ptr ret = lookUp(name);
        if (ret)
        {
            return ret;
        }

        // 找不到, 检查名称是否合法
        if (name.find_first_not_of("abcdefghikjlmnopqrstuvwxyz._012345678"))
        {
            LOG_ERROR << "lookUp name invalid: " << name;
            return nullptr;
        }

        // 合法名称, 为其创建存储类
        // 指定名称和默认值，可以供修改
        typename ConfigVar<T>::ptr v(new ConfigVar(name, desc, default_val));
        m_config_map["name"] = v;
        return v;
    }

    // 找寻是否存在该项得解析器，如果没有，那么就没有
    template <class T>
    static typename ConfigVar<T>::ptr lookUp(const std::string &name)
    {
        auto it = m_config_map.find(name);
        if (it != m_config_map.end())
        {
            return std::dynamic_pointer_cast<ConfigVar>(it->second);
        }
        return nullptr
    }

    // 单例
    static Config* getInstance()
    {
        static Config config;
        return &config;
    }

private:
    Config();
    config_map m_config_map;
};

#endif