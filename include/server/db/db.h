#ifndef __DB_H__
#define __DB_H__
#include <iostream>
#include <string>
#include <mysql/mysql.h>
#include <muduo/base/Logging.h>

using std::string;
using namespace muduo;

static string server = "127.0.0.1";
static string user = "test_chat";
static string password = "123456";
static string dbname = "chat";

// 数据库操作类
class MySQL
{
public:
    // 初始化数据库连接
    MySQL();
    // 释放数据库连接资源
    ~MySQL();
    // 连接数据库
    bool connect();

    // 更新操作
    bool update(string sql);

    // 查询操作
    MYSQL_RES *query(string sql);

    // 获取链接
    MYSQL* getConn();

private:
    MYSQL *_conn;
};

#endif